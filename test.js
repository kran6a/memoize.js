import memoizer from "./app.js";

const MEMO = memoizer({maxCacheSize: 100, ttl: 2500, activeVacuum: true, permanentCache: [6510n, 6500n, 6503n]});
const factorial = n=>{
    let ret = n;
    while (n > 1n){
        ret*=n;
        n--;
    }
    return ret;
}

const memofactorial = MEMO(factorial);
/*
let start, end;
start = Date.now();
memofactorial(BigInt(6500));
end = Date.now();
console.log("Non-memoized 6500", end-start, "ms");


start = Date.now();
factorial(BigInt(6500));
end = Date.now();
console.log("Memoize-warmup 6500", end-start, "ms");
*/
const values = [6510n, 6500n, 6503n];
let i = 0;
setInterval(()=>{
    const v = values[i];
    let start = Date.now();
    memofactorial(v);
    let end = Date.now();
    console.log("Memoized", /*memofactorial(v),*/ end-start, "ms");
    i = (i+1) %values.length;
}, 1100);
