import pqueue from "./pqueue.js";
const EMPTY = Symbol("empty");
export default (opts = {ttl: undefined, maxCacheSize: undefined, vacuumInterval: this?.ttl/2 || 0, activeVacuum: false, permanentCache: undefined, onDelete: undefined})=>{
    const cache = new Map();

    if (opts.permanentCache) {
        opts.permanentCache.forEach(k => {
            let key;
            try {
                key = JSON.stringify(k);
            } catch (e) {
                key = k.toString();
            }
            cache.set(key, EMPTY)
        }); //Won't be vacuumed
    }

    if ([...Object.values(opts)].every(v=>!v)) { //Default parameters. Does not use {value: n} to store results
        return fn => {
            return async (...args) => {
                let key;
                try {
                    key = JSON.stringify(args);
                } catch (e) {
                    key = args.toString();
                }
                let result = cache.get(key);
                if (!result || result === EMPTY) {
                    result = await fn(...args);
                    cache.set(key, result);
                }
                return result;
            };
        };
    }
    if (opts.ttl && opts.maxCacheSize){  //Time and size limited cache
        const TTLQueue = pqueue((a, b)=>a.ttl < b.ttl ? -1 : a.ttl > b.ttl ? 1 : 0);
        if (opts.activeVacuum) {
            setInterval(() => {
                while (TTLQueue.length > 0 && TTLQueue.peek()?.ttl < Date.now()) {
                    if (opts.onDelete)
                        opts.onDelete(cache.get(TTLQueue.peek().key).value);
                    cache.delete(TTLQueue.pop().key);
                }
            }, opts.vacuumInterval || opts.ttl);

            return fn => {
                return async (...args) => {
                    let key;
                    try {
                        key = JSON.stringify(args);
                    } catch (e) {
                        key = args.toString();
                    }
                    let result = cache.get(key);
                    if (result === EMPTY){
                        result = {value: await fn(...args)};
                        cache.set(key, result);
                        return result.value;
                    }
                    else if (!result) {
                        result = {value: await fn(...args), ttl: Date.now() + opts.ttl};
                        cache.set(key, result);
                        TTLQueue.push({key, ttl: result.ttl});

                        if (cache.size > opts.maxCacheSize) {
                            if (opts.onDelete)
                                opts.onDelete(cache.get(TTLQueue.peek().key).value);
                            cache.delete(TTLQueue.pop().key);
                        }
                        return result.value;
                    }
                    return result.value;
                };
            };
        }
        //!activeVacuum
        else{
            return fn => {
                return async (...args) => {
                    let key;
                    try {
                        key = JSON.stringify(args);
                    } catch (e) {
                        key = args.toString();
                    }
                    let result = cache.get(key);
                    if (result === EMPTY){
                        result = {value: await fn(...args)};
                        cache.set(key, result);
                    }
                    else if (!result) {
                        result = {value: await fn(...args), ttl: Date.now() + opts.ttl};
                        cache.set(key, result);
                        TTLQueue.push({key, ttl: result.ttl});
                        //Vacuuming on function call only
                        while (TTLQueue.length > 0 && TTLQueue.peek().ttl < Date.now()) {
                            if (opts.onDelete)
                                opts.onDelete(cache.get(TTLQueue.peek().key).value);
                            cache.delete(TTLQueue.pop().key);
                        }

                        if (cache.size > opts.maxCacheSize) {
                            if (opts.onDelete)
                                opts.onDelete(cache.get(TTLQueue.peek().key).value);
                            cache.delete(TTLQueue.pop().key);
                        }
                    }
                    return result.value;
                };
            };
        }
    }
    if (opts.ttl){  //Time limited cache
        return fn => {
            const TTLQueue = pqueue((a, b)=>a.ttl < b.ttl ? -1 : a.ttl > b.ttl ? 1 : 0);
            if (opts.activeVacuum) {
                setInterval(() => {
                    while (TTLQueue.length > 0 && TTLQueue.peek().ttl < Date.now()) {
                        if (opts.onDelete)
                            opts.onDelete(cache.get(TTLQueue.peek().key).value);
                        cache.delete(TTLQueue.pop().key);
                    }
                }, opts.vacuumInterval || opts.ttl);
                return async (...args) => {
                    let key;
                    try {
                        key = JSON.stringify(args);
                    } catch (e) {
                        key = args.toString();
                    }
                    let result = cache.get(key);
                    if (result === EMPTY){
                        result = {value: await fn(...args)};
                        cache.set(key, result);
                        return result.value;
                    }
                    else if (!result) {
                        result = {value: await fn(...args), ttl: Date.now() + opts.ttl};
                        TTLQueue.push({key, ttl: result.ttl});
                        cache.set(key, result);
                        return result.value;
                    }
                };
            }
            else{
                return async (...args) => {
                    let key;
                    try {
                        key = JSON.stringify(args);
                    } catch (e) {
                        key = args.toString();
                    }
                    //Vacuuming on function call only
                    while (TTLQueue.length > 0 && TTLQueue.peek().ttl < Date.now()) {
                        if (opts.onDelete)
                            opts.onDelete(cache.get(TTLQueue.peek().key).value);
                        cache.delete(TTLQueue.pop().key);
                    }
                    let result = cache.get(key);
                    if (result === EMPTY){
                        result = {value: await fn(...args)};
                        cache.set(key, result);
                    }
                    else if (!result) {
                        result = {value: await fn(...args), ttl: Date.now() + opts.ttl};
                        TTLQueue.push({key, ttl: result.ttl});
                        cache.set(key, result);
                    }
                    return result.value;
                };
            }
        };
    }
    if (opts.maxCacheSize) {  //Size limited cache
        return fn => {
            const KeyQueue = [];
            return async (...args) => {
                let key;
                try {
                    key = JSON.stringify(args);
                } catch (e) {
                    key = args.toString();
                }
                let result = cache.get(key);
                if (result === EMPTY){
                    result = {value: await fn(...args)};
                    cache.set(key, result);
                }
                else if (!result) {
                    result = {value: await fn(...args), ttl: Date.now() + opts.ttl};
                    cache.set(key, result);
                    KeyQueue.push(key);

                    if (cache.size >= opts.maxCacheSize) {
                        const key = KeyQueue.shift();
                        if (opts.onDelete)
                            opts.onDelete(cache.get(key).value);
                        cache.delete(key);
                    }
                }
                return result.value;
            };
        };
    }
}