const defaultCompare = (a, b) => {
    return a < b ? -1 : a > b ? 1 : 0;
}

export default (comparator = defaultCompare) => ({
    data: [],
    length: 0,
    push(item) {
        this.data.push(item);
        this._up(this.length++);
    },
    print(){
        this.data.forEach(d=>console.log(d))
    },
    pop(){
        if (this.length === 0)
            return undefined;

        const first = this.data[0];
        const last = this.data.pop();

        if (--this.length > 0) {
            this.data[0] = last;
            this._down(0);
        }

        return first;
    },
    peek(){
        return this.data[0] || undefined;
    },
    _up(pos) {
        const item = this.data[pos];

        while (pos > 0) {
            const parent = (pos - 1) >> 1;
            const current = this.data[parent];
            if (comparator(item, current) >= 0) break;
            this.data[pos] = current;
            pos = parent;
        }

        this.data[pos] = item;
    },
    _down(pos){
        while (pos < this.length >> 1) { // Length/2
            let bestChild = (pos << 1) + 1;
            const right = bestChild + 1;

            if (right < this.length && comparator(this.data[right], this.data[bestChild]) < 0)
                bestChild = right;

            if (comparator(this.data[bestChild], this.data[pos]) >= 0)
                return this.data[pos] = this.data[pos];

            this.data[pos] = this.data[bestChild];
            pos = bestChild;
        }
    }
});